package com.ibix.dao;

import com.ibix.domain.entity.User;
import com.ibix.service.IUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * 项目名称：bootdemo
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2018/1/15 17:21
 * <br>修改人:
 * <br>修改时间：2018/1/15 17:21
 * <br>修改备注：
 *
 * @author htliu
 * @date 2018/1/15 17:21
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserRepositoryTest.class);

    @Resource
    private BaseDao baseDao;

    @Resource
    private IUserService userService;

    @Test
    public void add() throws Exception {
        User user = new User();
        user.setName("amidn").setPhone("jdglkasjglsajk").setPassword("sagaasgagg").setId(4L);
        userService.updateUser(user);
    }

    @Test
    public void findByUserName() throws Exception {
        User user = baseDao.find(User.class, 1L);
        LOGGER.info("findByUserName:  user= " + user);
    }

    @Test
    public void findByUserNameOrEmail() throws Exception {
    }

}