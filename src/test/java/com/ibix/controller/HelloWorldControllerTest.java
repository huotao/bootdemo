package com.ibix.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ibix.domain.bean.ResultBean;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * 项目名称：bootdemo
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2018/1/12 10:35
 * <br>修改人:
 * <br>修改时间：2018/1/12 10:35
 * <br>修改备注：
 *
 * @author htliu
 * @date 2018/1/12 10:35
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class HelloWorldControllerTest {
    private MockMvc mockMvc;


    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.standaloneSetup(new HelloWorldController()).build();
    }

    @Test
    public void hello() throws Exception {
        ResultBean resultBean = new ResultBean().data("world");
        ObjectMapper mapper = new ObjectMapper();
        mockMvc.perform(MockMvcRequestBuilders.get("/hello/hi/world").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.valueToTree(resultBean).toString()));
    }

    @Test
    public void testHelloMan() throws Exception {
        PersonForm personForm = new PersonForm();
        personForm.setAge(16).setName("zjhangsan");
        mockMvc.perform(MockMvcRequestBuilders.post("/hello/man").content(personForm.toString())).andExpect(status().isOk());

    }
}