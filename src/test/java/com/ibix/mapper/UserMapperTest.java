package com.ibix.mapper;

import com.ibix.domain.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static org.junit.Assert.*;

/**
 * 项目名称：bootdemo
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2018/3/16 17:52
 * <br>修改人:
 * <br>修改时间：2018/3/16 17:52
 * <br>修改备注：
 *
 * @author htliu
 * @date 2018/3/16 17:52
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserMapperTest {
    @Resource
    UserMapper userMapper;

    @Test
    public void selectUserByID() {
        User user = userMapper.selectUserByID(4L);
        System.out.println("user = " + user);
    }
}