package com.ibix.service.impl;

import com.ibix.domain.dto.UserDTO;
import com.ibix.domain.entity.User;
import com.ibix.service.IUserService;
import com.ibix.util.JWT;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * 项目名称：bootdemo
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2018/1/16 10:08
 * <br>修改人:
 * <br>修改时间：2018/1/16 10:08
 * <br>修改备注：
 *
 * @author htliu
 * @date 2018/1/16 10:08
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceImplTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImplTest.class);
    @Resource
    private IUserService userService;

    @Resource
    private JWT jwt;

    @Resource
    private RedisTemplate redisTemplate;

    @Test
    public void updateUser() throws Exception {
        User user = new User();
        user.setName("test").setPhone("sgsjalgasjkgsjk");

        userService.updateUser(user);
    }

    @Test
    public void getUser() throws Exception {
        User user = userService.getUser(1);
        System.out.println("user = " + user);
        userService.getUser(1L);
    }

    @Test
    public void signUser() throws Exception {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(1L).setName("admin").setPhone("18286136813");
        System.out.println(jwt.sign(userDTO));
    }
}