package com.ibix.util;

import javax.servlet.http.HttpServletResponse;

/**
 * 项目名称：SmallShop
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2018/1/29 16:35
 * <br>修改人:
 * <br>修改时间：2018/1/29 16:35
 * <br>修改备注：
 *
 * @author htliu
 * @date 2018/1/29 16:35
 */
public class HttpUtil {

    /**
     * 添加cors支持
     *
     * @param response
     */
    public static void wrapCorsResponse(HttpServletResponse response) {
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
        response.addHeader("Access-Control-Allow-Headers", "Content-Type");
        response.addHeader("Access-Control-Max-Age", "1800");
    }
}
