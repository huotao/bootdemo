package com.ibix.constant;

/**
 * 项目名称：YiDianCan
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2017/9/19 11:01
 * <br>修改人:
 * <br>修改时间：2017/9/19 11:01
 * <br>修改备注：
 * <br>@version
 */
public class Constants {
    public static final String PARAM_TOKEN = "Access_token";
    public static final String PARAM_USERNAME = "User_name";
}
