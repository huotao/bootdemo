package com.ibix.controller;

import com.ibix.domain.bean.ResultBean;
import com.ibix.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 项目名称：bootdemo
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2018/1/12 10:30
 * <br>修改人:
 * <br>修改时间：2018/1/12 10:30
 * <br>修改备注：
 *
 * @author htliu
 * @date 2018/1/12 10:30
 */
@RestController
@RequestMapping("/hello")
public class HelloWorldController {
    private static final Logger LOGGER = LoggerFactory.getLogger(HelloWorldController.class);

    @Resource
    private Util util;

    @GetMapping("/hi/{s}")
    public ResultBean hello(@PathVariable String s) {
        LOGGER.info("hello " + "hello() called with: s = [" + s + "]");
        return new ResultBean().data(s);
    }

    @PostMapping("/man")
    public ResultBean helloMan(@RequestBody @Valid PersonForm personForm, BindingResult result) {
        LOGGER.info("helloMan:  personForm= " + personForm);
        util.checkResult(result);
        return new ResultBean();
    }

}
