package com.ibix.controller;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 项目名称：bootdemo
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2018/2/11 10:36
 * <br>修改人:
 * <br>修改时间：2018/2/11 10:36
 * <br>修改备注：
 *
 * @author htliu
 * @date 2018/2/11 10:36
 */
@Data
@Accessors(chain = true)
public class PersonForm {
    @NotNull
    @Size(min = 2, max = 30)
    private String name;

    @NotNull
    @Min(18)
    private Integer age;

}
