package com.ibix.controller;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 项目名称：bootdemo
 * <br>类描述：在进入Controller之前，譬如请求一个不存在的地址，404错误。
 * <br>创建人：htliu
 * <br>创建时间：2018/2/11 14:12
 * <br>修改人:
 * <br>修改时间：2018/2/11 14:12
 * <br>修改备注：
 *
 * @author htliu
 * @date 2018/2/11 14:12
 */
@RestController
public class FinalExceptionHandler implements ErrorController {
    @Override
    public String getErrorPath() {
        return "/error";
    }

    @RequestMapping(value = "/error")
    public Object error(HttpServletResponse resp, HttpServletRequest req) {
        // 错误处理逻辑
        return "其他异常";
    }
}
