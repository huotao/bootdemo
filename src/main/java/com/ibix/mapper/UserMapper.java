package com.ibix.mapper;

import com.ibix.domain.entity.User;
import org.apache.ibatis.annotations.Mapper;

import javax.persistence.ManyToOne;

/**
 * 项目名称：bootdemo
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2018/3/16 17:48
 * <br>修改人:
 * <br>修改时间：2018/3/16 17:48
 * <br>修改备注：
 *
 * @author htliu
 * @date 2018/3/16 17:48
 */
@Mapper
public interface UserMapper {
    User selectUserByID(long id);
}
