package com.ibix.shiro;

import com.ibix.constant.Constants;
import com.ibix.domain.bean.StatelessToken;
import com.ibix.util.HttpUtil;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 项目名称：CaiHai
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2017/9/19 10:57
 * <br>修改人:
 * <br>修改时间：2017/9/19 10:57
 * <br>修改备注：
 * <br>@version
 */
public class StatelessAuthcFilter extends AccessControlFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(StatelessAuthcFilter.class);

    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
        return false;
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        if ("OPTIONS".equals(httpServletRequest.getMethod())) {
            return true;
        }
        //1、客户端生成的消息摘要
        String accessToken = httpServletRequest.getHeader(Constants.PARAM_TOKEN);
        //2、客户端传入的用户身份
        String username = httpServletRequest.getHeader(Constants.PARAM_USERNAME);
        //4、生成无状态Token
        StatelessToken token = new StatelessToken(username, accessToken);
        try {
            //5、委托给Realm进行登录
            getSubject(request, response).login(token);
        } catch (Exception e) {
            e.printStackTrace();
            //6、登录失败
            onLoginFail(response);
            return false;
        }
        return true;
    }

    /**
     * 登录失败时默认返回401状态码
     *
     * @param response
     * @throws IOException
     */
    private void onLoginFail(ServletResponse response) throws IOException {
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpUtil.wrapCorsResponse(httpResponse);
        httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        httpResponse.getWriter().write("login error");
    }
}
