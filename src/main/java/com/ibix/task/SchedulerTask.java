package com.ibix.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 项目名称：bootdemo
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2018/2/5 16:51
 * <br>修改人:
 * <br>修改时间：2018/2/5 16:51
 * <br>修改备注：
 *
 * @author htliu
 * @date 2018/2/5 16:51
 */
@Component
public class SchedulerTask {

    private int count = 0;

    @Scheduled(cron = "*/6 * * * * ?")
    private void process() {
//        System.out.println("this is scheduler task runing  " + (count++));
    }

}
