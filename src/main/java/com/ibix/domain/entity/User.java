package com.ibix.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * 项目名称：SmallShop
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2017/12/12 10:47
 * <br>修改人:
 * <br>修改时间：2017/12/12 10:47
 * <br>修改备注：
 *
 * @author htliu
 * @date 2017/12/12 10:47
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Table
@Data
@Accessors(chain = true)
public class User extends BaseEntity {
    private static final long serialVersionUID = 1L;

    @Column(length = 30)
    private String name;

    @Column(unique = true, length = 20, nullable = false)
    private String phone;

    @Column
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    @JsonIgnore
    private Set<Role> roles = new HashSet<>();

}
