package com.ibix.domain.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * 项目名称：SmallShop
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2017/9/4 16:35
 * <br>修改人:
 * <br>修改时间：2017/9/4 16:35
 * <br>修改备注：
 * <br>@version
 */
@Entity
@Table
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Permission extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Column(nullable = false, unique = true)
    @NotNull
    private String name;


}
