package com.ibix.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

/**
 * 项目名称：SmallShop
 * <br>类描述：角色
 * <br>创建人：htliu
 * <br>创建时间：2017/9/4 16:31
 * <br>修改人:
 * <br>修改时间：2017/9/4 16:31
 * <br>修改备注：
 * <br>@version
 */
@Entity
@Table
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Role extends BaseEntity {
    private static final long serialVersionUID = 1L;

    @Column(unique = true, nullable = false)
    @NotNull
    private String name;

    @Column(length = 20)
    private String nickName;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(joinColumns = @JoinColumn(name = "role_id"), inverseJoinColumns = @JoinColumn(name = "permission_id"))
    @JsonIgnore
    private Set<Permission> permissions = new HashSet<>();

}
