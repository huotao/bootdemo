package com.ibix.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;
import java.util.Date;

/**
 * 项目名称：SmallShop
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2017/11/15 17:54
 * <br>修改人:
 * <br>修改时间：2017/11/15 17:54
 * <br>修改备注：
 *
 * @author htliu
 * @date 2017/11/15 17:54
 */
@Data
@Accessors(chain = true)
public class UserDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private long id;
    @NotBlank
    private String phone;
    @NotBlank
    private String password;

    private String name;


    private String token;

    private String refreshToken;


    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date modifyTime;


}
