package com.ibix.domain.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 项目名称：bootdemo
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2018/2/11 10:53
 * <br>修改人:
 * <br>修改时间：2018/2/11 10:53
 * <br>修改备注：
 *
 * @author htliu
 * @date 2018/2/11 10:53
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Accessors(chain = true)
public class User {

    private String name;
    private String blog;
}
