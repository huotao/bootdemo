package com.ibix.domain.bean;

import lombok.Data;

/**
 * 项目名称：bootdemo
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2018/2/11 10:16
 * <br>修改人:
 * <br>修改时间：2018/2/11 10:16
 * <br>修改备注：
 *
 * @author htliu
 * @date 2018/2/11 10:16
 */
@Data
public class Book {
    private Long id;
    private String name;
    private long price;
}
