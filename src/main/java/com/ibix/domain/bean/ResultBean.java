package com.ibix.domain.bean;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 项目名称：CaiHai
 * <br>类描述：统一返回数据
 * <br>创建人：htliu
 * <br>创建时间：2017/9/15 16:49
 * <br>修改人:
 * <br>修改时间：2017/9/15 16:49
 * <br>修改备注：
 * <br>@version
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@Data
@Accessors(fluent = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResultBean<T> implements Serializable {
    public static final int SUCCESS = 0;
    private static final long serialVersionUID = 1L;
    /**
     * 默认设置 SUCCESS
     */
    private int status = SUCCESS;
    private String message = "SUCCESS";
    private T data;

    public ResultBean<T> data(Object data) {
        this.data = (T) data;
        return this;
    }
}
