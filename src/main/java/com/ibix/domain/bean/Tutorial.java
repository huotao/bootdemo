package com.ibix.domain.bean;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 项目名称：bootdemo
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2018/2/11 13:30
 * <br>修改人:
 * <br>修改时间：2018/2/11 13:30
 * <br>修改备注：
 *
 * @author htliu
 * @date 2018/2/11 13:30
 */
@Data
@Accessors(chain = true)
public class Tutorial implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private String name;

}
