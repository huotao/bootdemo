package com.ibix.domain.bean;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 项目名称：SmallShop
 * <br>类描述：封装查询的参数
 * <br>创建人：htliu
 * <br>创建时间：2017/12/14 16:35
 * <br>修改人:
 * <br>修改时间：2017/12/14 16:35
 * <br>修改备注：
 *
 * @author htliu
 * @date 2017/12/14 16:35
 */
@Data
@Accessors(fluent = true)
public class QueryParam {
    private Class clazz;
    private Object primaryKey;

    private String sql;
    private String hql;
    private List<Object> params = new ArrayList<>();

    private String errorMessage;

    private Type type;

    public QueryParam(Class clazz, String sql) {
        this.clazz = clazz;
        this.sql = sql;
        type = Type.SQL;
    }

    public QueryParam(Class clazz, Object primaryKey) {
        this.clazz = clazz;
        this.primaryKey = primaryKey;
        type = Type.PRIMARY_KEY;
    }

    public QueryParam(String hql) {
        this.hql = hql;
        type = Type.HQL;
    }

    public QueryParam addParam(Object... params) {
        this.params.addAll(Arrays.asList(params));
        return this;
    }

    /**
     * 查询类型
     */
    public enum Type {
        SQL,
        HQL,
        PRIMARY_KEY,
    }
}
