package com.ibix.service.impl;

import com.ibix.service.BaseService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 项目名称：CaiHai
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2017/9/6 14:28
 * <br>修改人:
 * <br>修改时间：2017/9/6 14:28
 * <br>修改备注：
 * <br>@version
 */
@Service
public class AuthorizationService extends BaseService {

    public List<String> listRoles(String userName) {
        String sql = "SELECT r.name from user_roles ur join user u on  ur.user_id=u.id and u.phone=? JOIN role r on ur.role_id=r.id ";
        List list = listBySql(sql, userName);
        return list;
    }

    public List<String> listPermissions(String roleName) {
        String sql = "SELECT p.name from role_permissions rp join role r on  rp.role_id =r.id and r.name=?  join  permission p on rp.permission_id =p.id";
        List list = listBySql(sql, roleName);
        return list;
    }

}
