package com.ibix.service.impl;

import com.ibix.domain.bean.ResultBean;
import com.ibix.domain.entity.User;
import com.ibix.service.BaseService;
import com.ibix.service.IUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * 项目名称：bootdemo
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2018/1/16 10:05
 * <br>修改人:
 * <br>修改时间：2018/1/16 10:05
 * <br>修改备注：
 *
 * @author htliu
 * @date 2018/1/16 10:05
 */
@Service
public class UserServiceImpl extends BaseService implements IUserService {
    @Override
    public ResultBean updateUser(User user) {
        User useDB = find(User.class, user.getId());
        BeanUtils.copyProperties(user, useDB, wrapIgnores());
        return new ResultBean();
    }

    @Cacheable("user")
    @Override
    public User getUser(long id) {
        User user = find(User.class, id);
        update(user);
        System.out.println("UserServiceImpl.getUser call ");
        return user;
    }

    @CacheEvict("user")
    @Override
    public void deleteUser(long id) {
        System.out.println("UserServiceImpl.deleteUser");
    }
}
