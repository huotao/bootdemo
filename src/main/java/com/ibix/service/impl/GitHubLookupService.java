package com.ibix.service.impl;

import com.ibix.domain.bean.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.concurrent.Future;

/**
 * 项目名称：bootdemo
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2018/2/11 10:53
 * <br>修改人:
 * <br>修改时间：2018/2/11 10:53
 * <br>修改备注：
 *
 * @author htliu
 * @date 2018/2/11 10:53
 */
@Service
public class GitHubLookupService {

    private static final Logger logger = LoggerFactory.getLogger(GitHubLookupService.class);

    @Resource
    private RestTemplate restTemplate;


    @Async
    public Future<User> findUser(String user) throws InterruptedException {
        logger.info("Looking up " + Thread.currentThread() + user);
        String url = String.format("https://api.github.com/users/%s", user);
        User results = restTemplate.getForObject(url, User.class);
        // Artificial delay of 1s for demonstration purposes
        Thread.sleep(1000L);
        return new AsyncResult<>(results);
    }

}
