package com.ibix.service;

import com.ibix.domain.bean.ResultBean;
import com.ibix.domain.entity.User;

/**
 * 项目名称：bootdemo
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2018/1/16 10:03
 * <br>修改人:
 * <br>修改时间：2018/1/16 10:03
 * <br>修改备注：
 *
 * @author htliu
 * @date 2018/1/16 10:03
 */
public interface IUserService {
    ResultBean updateUser(User user);

    User getUser(long id);

    void deleteUser(long id);
}
