package com.ibix.dao;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * 项目名称：CaiHai
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2017/8/17 15:09
 * <br>修改人:
 * <br>修改时间：2017/8/17 15:09
 * <br>修改备注：
 * <br>@version
 */
@Repository
public class BaseDao {
    @PersistenceContext
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public <T> void add(T data) {
        entityManager.persist(data);
    }

    public <T> void merge(T data) {
        entityManager.merge(data);
    }

    public <T> T find(Class<T> clazz, Object primaryKey) {
        return entityManager.find(clazz, primaryKey);
    }

    /**
     * @param hql
     * @param parameters
     * @param <T>
     * @return null 没有查到
     */
    public <T> T find(String hql, Object... parameters) {
        Query query = entityManager.createQuery(hql);
        addParameters(query, parameters);
        try {
            return (T) query.getSingleResult();
        } catch (NoResultException e) {
        }
        return null;
    }

    public <T> void delete(Class<T> clazz, Object primaryKey) {
        T data = find(clazz, primaryKey);
        entityManager.remove(data);
    }

    public void delete(String hql, Object... parameters) {
        Object data = find(hql, parameters);
        entityManager.remove(data);
    }

    public void exceuteSql(String sql, Object... parameters) {
        Query query = entityManager.createNativeQuery(sql);
        addParameters(query, parameters);
        query.executeUpdate();
    }


    public <T> List<T> listPageByHql(String hql, int page, int limit, Object... parameters) {
        Query query = entityManager.createQuery(hql);
        addPage(page, limit, query);
        addParameters(query, parameters);
        return query.getResultList();
    }

    public <T> List<T> listLimitByHql(String hql, int limit, Object... parameters) {
        Query query = entityManager.createQuery(hql);
        query.setMaxResults(limit);
        addParameters(query, parameters);
        return query.getResultList();
    }

    public <T> List<T> listByHql(String hql, Object... parameters) {
        Query query = entityManager.createQuery(hql);
        addParameters(query, parameters);
        return query.getResultList();
    }

    public List<Object> listBySql(String sql, Object... parameters) {
        Query query = entityManager.createNativeQuery(sql);
        addParameters(query, parameters);
        return query.getResultList();
    }

    public <T> List<T> listBySql(Class<T> clazz, String sql, Object... parameters) {
        Query query = entityManager.createNativeQuery(sql, clazz);
        addParameters(query, parameters);
        return query.getResultList();
    }

    public <T> List<T> listPageBySql(String sql, int page, int limit, Object... parameters) {
        Query query = entityManager.createNativeQuery(sql);
        addPage(page, limit, query);
        addParameters(query, parameters);
        return query.getResultList();
    }

    public <T> List<T> listPageBySql(Class<T> clazz, String sql, int page, int limit, Object... parameters) {
        Query query = entityManager.createNativeQuery(sql, clazz);
        addPage(page, limit, query);
        addParameters(query, parameters);
        return query.getResultList();
    }

    private void addPage(int page, int limit, Query query) {
        query.setFirstResult((page - 1) * limit);
        query.setMaxResults(limit);
    }

    private void addParameters(Query query, Object... parameters) {
        for (int i = 0; i < parameters.length; i++) {
            query.setParameter(i + 1, parameters[i]);
        }
    }


}
