package com.ibix.dao;

import com.ibix.domain.bean.Article;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * 项目名称：bootdemo
 * <br>类描述：泛型的参数分别是实体类型和主键类型
 * <br>创建人：htliu
 * <br>创建时间：2018/2/11 13:38
 * <br>修改人:
 * <br>修改时间：2018/2/11 13:38
 * <br>修改备注：
 *
 * @author htliu
 * @date 2018/2/11 13:38
 */
public interface ArticleSearchRepository extends ElasticsearchRepository<Article, Long> {

}
